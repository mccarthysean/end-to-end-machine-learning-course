#!/bin/bash

# Use netcat (nc) to check if the TimescaleDB host/port are accessible
echo "Waiting for postgres..."
while ! nc -z timescale_stocks_ml 5432; do
  sleep 0.1
done
echo "PostgreSQL started. Starting Gunicorn..."

# Finally, start the Gunicorn app server for the Flask app
gunicorn --config gunicorn-cfg.py wsgi:app
