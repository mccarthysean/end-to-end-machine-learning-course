from selenium.webdriver.chrome.options import Options


def pytest_setup_options():
    """pytest extra command line arguments for running
    in a Debian Docker container"""

    options = Options()
    options.add_argument("--disable-gpu")
    options.add_argument("--no-sandbox")
    options.add_argument("--headless")

    return options
