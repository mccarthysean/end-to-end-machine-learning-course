import pytest

from app import create_app, db
from app.database import check_db_tables
from app.models import User


@pytest.fixture(scope="module")
def flask_and_dash_tuple():
    """
    pytest fixture to share the Flask app
    among different tests, if desired
    """

    app, dashapp = create_app()

    # Disable CSRF protection for the unit tests
    app.config['WTF_CSRF_ENABLED'] = False

    with app.app_context():
        # This is where the testing happens
        yield app, dashapp


@pytest.fixture(scope="module")
def init_database(flask_and_dash_tuple):
    """Initialize the testing database"""

    flask_app, dashapp = flask_and_dash_tuple

    # Create the database and the database tables if necessary
    check_db_tables(flask_app, db)
 
    # Insert a demo user so we can login
    user = User.query.filter_by(email="demo@test.com").first()
    if not user:
        user = User(
            email="demo@test.com",
            password="password",
            first_name="Demo",
            last_name="User",
        )
        db.session.add(user)
    
        # Commit the changes to the database
        db.session.commit()
 
    # This is where the testing happens
    yield db


def test_main_flask_route(flask_and_dash_tuple):
    """
    Very simple Flask test to see if the main
    Flask route is accessible
    """

    flask_app, dashapp = flask_and_dash_tuple
    test_client = flask_app.test_client()
    response = test_client.get("/")
    assert response.status_code == 200
    assert b'Click <a href="/dash/">here</a> to see the Dash single-page application (SPA)' in response.data


def test_login_and_logout(flask_and_dash_tuple, init_database):
    """
    Given a Flask application,
    when the "/login" page is posted to (POST),
    check the response is valid
    """
    
    flask_app, dashapp = flask_and_dash_tuple
    test_client = flask_app.test_client()
    response = test_client.post(
        "/login/",
        data=dict(
            email="demo@test.com",
            password="password"
        ),
        follow_redirects=True
    )
    assert response.status_code == 200

    # The 'react-entry-point' ID is in the Dash app, where we get redirected after login
    assert b'id="react-entry-point"' in response.data
 
    """
    Given a Flask application,
    when the "/logout" page is requested (GET),
    check the response is valid
    """
    response = test_client.get("/logout/", follow_redirects=True)
    assert response.status_code == 200

    # We should be redirected back to the simple homepage
    assert b'Click <a href="/dash/">here</a> to see the Dash single-page application (SPA)' in response.data
