import os
import pathlib
import sys
import time

import dash
import pytest

from app import create_app
from app.dashapp.utils import ml_features_map, ml_models_map
from tests.test_flask import flask_and_dash_tuple, init_database


def test_dash_app(dash_br, dash_thread_server, flask_and_dash_tuple, init_database):
    """
    Test our Dash app in headless Chrome using Selenium WebDriver.

    "dash_br" is a pytest fixture, included in the Dash package, with
    methods for selecting Dash elements in the DOM.

    "dash_thread_server" is also a Dash pytest fixture for running the
    Dash app in a lightweight threaded server
    """

    flask_app, dashapp = flask_and_dash_tuple

    # Start the lightweight threaded server with our Dash app,
    # and pass optional Dash arguments
    dash_thread_server.start(dashapp, host="0.0.0.0", port=5000)

    # Update the server_url to include our "/dash/" prefix for our app.
    extended_url = dash_thread_server.url + dashapp.config.url_base_pathname
    # Set the main _url property
    dash_br._url = extended_url

    # Now that the threaded server is started, use the Selenium WebDriver "browser"
    # to first log in and then get redirected to the /dash/ page
    dash_br.driver.get("http://localhost:5000/login/")

    # Find the email and password form inputs (id="email" and id="password")
    email = dash_br.find_element("#email")
    password = dash_br.find_element("#password")

    # "Type" the demo user login credentials into the form input fields
    email.send_keys("demo@test.com")
    password.send_keys("password")

    # Find the "submit" button and click it to login
    # and be redirected to the "/dash/" page
    dash_br.find_element("#submit").click()
    
    # Wait for the Dash layout element with id='#react-entry-point'
    # so we know the Dash single page application has fully rendered/loaded
    dash_br.wait_for_element_by_css_selector(
        dash_br.dash_entry_locator, timeout=3
    )

    # Ensure our initial layout is loaded
    assert (
        dash_br.wait_for_text_to_equal("h4", "Pick an Industry", timeout=2) is True
    ), "Check if 'Pick an Industry' is rendered"

    # Our first DOM test. Assert our dropdown menu contains the following text
    text_we_expect = (
        "Logistic Regression\nRidge Classifier\nK-Nearest Neighbors\n"
        + "AdaBoost Decision Tree\nRandom Forest\nSupport Vector Machine\nNeural Network"
    )
    element = dash_br.find_element("#ml_models_radio")
    assert (
        element.text == text_we_expect
    ), "ml_models_radio contains our ML model options"

    # Click on the "Logistic Regression" radio item using the Selenium WebDriver
    dash_br.driver.find_element_by_xpath(
        "//label[contains(.,'Logistic Regression')]"
    ).click()

    # Add a stock to the download input, and click the download button
    stock_uploaded_msg = dash_br.find_element("#stock_uploaded_msg")
    assert stock_uploaded_msg.text == "", "no message yet under the download button"

    new_stock_input = dash_br.find_element("#add_stock_input")
    # Ensure there's no text in the input field
    new_stock_input.clear()
    ticker = "mrna"
    new_stock_input.send_keys(ticker)

    download_button = dash_br.find_element("#add_stock_input_button")
    download_button.click()
    # This call requires a few seconds to download the historical stock price data
    dash_br.wait_for_text_to_equal(
        "#stock_uploaded_msg", f"{ticker.upper()} downloaded!", timeout=5
    )

    # Click the "Train Model" button to start the training/analysis
    dash_br.find_element("#train_ml_btn").click()

    # Check that the machine learning model has completed
    dash_br.wait_for_contains_text(
        "#model_trained_msg",
        f"{ticker.upper()} logistic regression model trained in ",
        timeout=5,
    )

    # Check that the machine learning model has completed, and has calculated
    # a testing period ending value
    text_we_expect = "Testing Period Ending Value:"
    assert (
        text_we_expect in dash_br.find_element("#profits_chart_msg_div").text
    ), f"Check that {text_we_expect} is visible now"

    # Save a screenshot of what we see
    filename = "screenshot.png"
    # Remove any existing file
    if os.path.exists(filename):
        os.remove(filename)
    assert dash_br.driver.save_screenshot(filename) is True, "Save a screenshot"

    # Optional: Assert there are no errors in the browser console
    # Note: get_logs always returns None with webdrivers other than Chrome
    assert dash_br.get_logs() == [], "no errors in the browser console"
