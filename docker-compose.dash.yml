version: '3.7'
services:
  stocks_ml:
    # Name and tag of image the Dockerfile creates
    image: mccarthysean/stocks_ml:latest
    env_file: .env
    environment:
      FLASK_CONFIG: production
      FLASK_ENV: production
      FLASK_DEBUG: 0
    networks:
      - traefik-public
      - timescale_network
    healthcheck:
      # Command to check if the container is running, for zero-downtime deployment.
      # If the website is fine, curl returns a return code of 0 and deployment continues
      # NOTE: must have curl installed in the stocks_ml Docker container
      test: ["CMD", "curl", "-i", "http://localhost:5000/healthcheck"]
    deploy:
      # Either global (exactly one container per physical node) or
      # replicated (a specified number of containers). The default is replicated
      mode: replicated
      # For stateless applications using "replicated" mode,
      # the total number of replicas to create
      replicas: 1
      update_config:
        # parallelism = the number of containers to update at a time
        parallelism: 1
        # start-first = new task is started first, and the running tasks briefly overlap
        order: start-first
        # What to do if an update fails
        failure_action: rollback
        # time to wait between updating a group of containers
        delay: 5s
      rollback_config:
        # If parallelism set to 0, all containers rollback simultaneously
        parallelism: 0
        # stop-first = old task is stopped before starting new one
        order: stop-first
      restart_policy:
        condition: on-failure
      labels:
        # Ensure Traefik sees it and does Letsencrypt for HTTPS
        - traefik.enable=true
        # Must be on traefik-public overlay Docker Swarm network
        - traefik.docker.network=traefik-public
        - traefik.constraint-label=traefik-public
        # HTTP (port 80)
        - traefik.http.routers.stocks_ml-http.rule=Host(`stocksmldemo.mccarthysean.dev`)
        - traefik.http.routers.stocks_ml-http.entrypoints=http
        - traefik.http.routers.stocks_ml-http.middlewares=https-redirect
        # HTTPS (port 443)
        - traefik.http.routers.stocks_ml-https.rule=Host(`stocksmldemo.mccarthysean.dev`)
        - traefik.http.routers.stocks_ml-https.entrypoints=https
        - traefik.http.routers.stocks_ml-https.tls=true
        - traefik.http.routers.stocks_ml-https.tls.certresolver=le
        # Application-specific port
        - traefik.http.services.stocks_ml.loadbalancer.server.port=5000

networks:
  # For the TimescaleDB database
  timescale_network:
    external: true
  # For the Traefik web server
  traefik-public:
    external: true
