import datetime

from flask import flash, redirect, render_template, request, url_for
from flask_login import current_user, login_required, login_user, logout_user


def _redirect():
    """Finds where to redirect the user"""

    next_url = request.args.get("next")
    if next_url:
        return redirect(next_url)

    return redirect("/dash/")


def register_views(app):
    """Register all views with the Flask app"""

    app.logger.info("Registering views...")

    # Import our forms and User model inside the function,
    # to avoid a common Flask circular reference error
    from app import db
    from app.database import run_sql_query
    from app.forms import LoginForm, RegistrationForm
    from app.models import User

    @app.route("/")
    def home():
        """Super-simple non-Dash route,
        to demonstrate that Flask can be used normally"""
        return render_template("index.html")

    @app.route("/healthcheck/")
    def healthcheck():
        """Check that the app is up and running,
        for Docker Swarm zero-downtime deployment.
        This endpoint is monitored by UptimeRobot, a free monitoring service"""

        run_sql_query("select ticker from public.stock_prices limit 1")

        return "The app and database are working fine"

    @app.route("/register/", methods=["GET", "POST"])
    def register():
        """View for new user registration"""

        # If the method is a POST request and the form validates,
        # register the new user
        form = RegistrationForm()
        if form.validate_on_submit():
            # Convert the submitted email address to lowercase first
            email_lower = form.email.data.lower()

            user = User(
                email=email_lower,
                password=form.password.data,
                first_name=form.first_name.data,
                last_name=form.last_name.data,
            )

            # Add the new user to the database
            db.session.add(user)
            db.session.commit()
            flash("You have successfully registered! You may now login.")
            return redirect(url_for("login"))

        return render_template("register.html", form=form)

    @app.route("/login/", methods=["GET", "POST"])
    def login():
        """View for user login"""

        if current_user.is_authenticated:
            # If the user is already authenticated
            _redirect()

        form = LoginForm()
        if form.validate_on_submit():
            # Check whether the password entered matches the password in the database
            user = User.query.filter_by(email=form.email.data.lower()).first()
            if user and user.verify_password(form.password.data):
                # Save some details about this login, and previous logins
                user.last_login_at = datetime.datetime.utcnow()
                user.login_count = (
                    1 if user.login_count is None else user.login_count + 1
                )
                db.session.commit()

                # Log user in
                remember_me = form.remember_me.data
                remember_me_for = datetime.timedelta(days=1)
                login_user(user, remember=remember_me, duration=remember_me_for)

                # redirect to the appropriate dashboard page
                return _redirect()

            else:
                # When login details are incorrect
                flash("Invalid email or password")

        return render_template("login.html", form=form)

    @app.route("/logout/")
    @login_required
    def logout():
        """This is what happens when the user logs out"""

        logout_user()
        return redirect(url_for("home"))
