from flask_login import UserMixin
from sqlalchemy.dialects.postgresql import INTEGER, NUMERIC, TEXT, TIMESTAMP
from sqlalchemy.orm import relationship
from werkzeug.security import check_password_hash, generate_password_hash

from app import db, login_manager


class StockTicker(db.Model):
    """
    Model for public.stock_tickers table,
    so it can be created with db.create_all(),
    or destroyed with db.drop_all()
    """
    __tablename__ = "stock_tickers"
    __table_args__ = {"schema": "public"}

    ticker = db.Column(TEXT, primary_key=True)
    name = db.Column(TEXT)
    industry = db.Column(TEXT)

    stock_prices = relationship("StockPrice", back_populates="ticker_rel")


class StockPrice(db.Model):
    """
    Model for public.stock_prices table,
    so it can be created with db.create_all(),
    or destroyed with db.drop_all()
    """
    __tablename__ = "stock_prices"
    __table_args__ = {"schema": "public"}
    
    time = db.Column(TIMESTAMP, primary_key=True, nullable=False)
    
    ticker = db.Column(
        TEXT,
        db.ForeignKey("public.stock_tickers.ticker"),
        nullable=True
    )
    ticker_rel = relationship("StockTicker", back_populates="stock_prices")

    open = db.Column(NUMERIC)
    high = db.Column(NUMERIC)
    low = db.Column(NUMERIC)
    close = db.Column(NUMERIC)
    close_adj = db.Column(NUMERIC)
    volume = db.Column(NUMERIC)


class User(UserMixin, db.Model):
    """Create a User model for the "public.users" database table"""

    __tablename__ = "users"

    id = db.Column(INTEGER, primary_key=True)
    email = db.Column(TEXT, unique=True, nullable=False)
    first_name = db.Column(TEXT, nullable=False)
    last_name = db.Column(TEXT, nullable=False)
    password_hash = db.Column(TEXT, nullable=False)

    last_login_at = db.Column(TIMESTAMP)
    login_count = db.Column(INTEGER)

    @property
    def password(self):
        """Prevent password from being accessed"""
        raise AttributeError("password is not a readable attribute.")

    @password.setter
    def password(self, password):
        """Set password to a hashed password"""
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        """Check if hashed password matches actual password"""
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return str(self.email)


@login_manager.user_loader
def load_user(user_id):
    """Set up user_loader for Flask-Login"""
    return User.query.get(int(user_id))
