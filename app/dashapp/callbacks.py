# /app/dashapp/callbacks.py
import time

import dash
import dash_html_components as html
import numpy as np
import plotly.graph_objs as go
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
from flask import current_app

from app.dashapp.ml import feature_engineering, train_models
from app.dashapp.utils import (
    download_prices,
    get_chart,
    get_stock_industries,
    get_stock_price_data_from_db,
    get_stock_tickers,
    get_time_series_chart,
    make_annotations,
    ml_models_map,
)


def register_callbacks(dash_app):
    """Register the callback functions for the Dash app, within the Flask app"""

    @dash_app.callback(
        Output("tickers_dropdown", "options"), [Input("industries_dropdown", "value")]
    )
    def get_stocks_from_industries_dropdown(industries_dropdown_value):
        """Get the stocks available, based on the industry chosen"""

        stocks_options, _ = get_stock_tickers(industries_dropdown_value)

        return stocks_options

    @dash_app.callback(
        [
            Output("stock_uploaded_msg", "children"),
            Output("industries_dropdown", "options"),
            Output("industries_dropdown", "value"),
            Output("tickers_dropdown", "value"),
        ],
        [Input("add_stock_input_button", "n_clicks")],
        [State("add_stock_input", "value")],
    )
    def get_new_stock_information(add_stock_input_button_clicks, add_stock_input_value):
        """Add a stock's historical data to the database"""

        if add_stock_input_button_clicks is None or add_stock_input_value is None:
            # If this callback fires with no data, stop execution
            raise PreventUpdate

        # First get the location options (i.e. a list of dictionaries)
        ticker_upper = str(add_stock_input_value).upper()
        try:
            industry_chosen = download_prices(ticker_upper)
        except KeyError:
            current_app.logger.exception("Trouble finding stock information...")
            msg = f"{ticker_upper} didn't work! Please try a different stock... :("
            return msg, dash.no_update, dash.no_update, dash.no_update

        msg = f"{ticker_upper} downloaded!"
        industries, _ = get_stock_industries()

        return msg, industries, industry_chosen, ticker_upper

    @dash_app.callback(
        Output("time_series_chart_div", "children"),
        [Input("tickers_dropdown", "value")],
    )
    def get_normal_time_series_chart(tickers_dropdown_value):
        """Get the normal time series chart of the stock price history"""

        return get_time_series_chart(tickers_dropdown_value)

    @dash_app.callback(
        [
            Output("model_trained_msg", "children"),
            Output("ml_chart_div", "children"),
            Output("profits_chart_div", "children"),
            Output("profits_chart_msg_div", "children"),
        ],
        [Input("train_ml_btn", "n_clicks"), Input("tickers_dropdown", "value")],
        [
            State("ml_models_radio", "value"),
            State("ml_cross_validation_splits", "value"),
            State("ml_features_to_use", "value"),
            State("ml_do_hyper_param_tuning", "value"),
            State("train_test_date_picker", "date"),
            State("ml_strategy", "value"),
        ],
    )
    def train_machine_learning_model(
        train_ml_btn_clicks,
        ticker,
        ml_model,
        n_splits,
        features,
        hyper_tune,
        date_test,
        strategy,
    ):
        """Get the stocks available, based on the industry chosen"""

        if (
            ticker is None
            or ml_model is None
            or n_splits is None
            or features is None
            or hyper_tune is None
            or date_test is None
            or strategy is None
        ):
            raise PreventUpdate

        # This callback might take a while. Let's time it,
        # and display the elapsed time on the website
        time_start = time.time()

        # Get stock price data from TimescaleDB database
        df = get_stock_price_data_from_db(ticker)

        # Create machine learning features (explanatory variables)
        df = feature_engineering(df)

        # Make train/test splits, so we can test on data that's never been seen before
        df_train = df[df["time"] < date_test].copy()
        current_app.logger.info(f"df_train.shape: {df_train.shape}")

        # Isolate the "features" or "explanatory variables" from
        # the value we're trying to predict (tomorrow's returns)
        if "rt" in features:
            # Add the other lagged returns
            features += ("rt1", "rt2", "rt3", "rt4", "rt5")
        X = df[features].values
        X_train = df_train[features].values

        # Isolate the value we're trying to predict
        # y_feature = 'up_down_tm'
        y_feature = "up_down_1wk"
        df[y_feature].values
        y_train = df_train[y_feature].values

        # Train the machine learning model on the training data
        estimator, _ = train_models(
            X_train, y_train, n_splits, features, ml_model, hyper_tune
        )

        # Add predictions to DataFrame, so we can chart them
        df["pred"] = estimator.predict(X)

        # If our prediction == 1, buy 100 shares.
        # Otherwise either short-sell 100 shares, or exit the position
        n_shares = 100
        if strategy == "lo":
            # Long-only strategy (no short-selling)
            df["position"] = np.where(df["pred"] == 1, n_shares, 0)
        else:
            # Allow short-selling
            df["position"] = np.where(df["pred"] == 1, n_shares, -n_shares)

        # Make buy and sell flags based on the position
        df["buy"] = np.where(
            (df["position"] == n_shares) & (df["position"].shift(1) != n_shares), 1, 0
        )
        df["sell"] = np.where(
            (df["position"] != n_shares) & (df["position"].shift(1) == n_shares), 1, 0
        )

        traces_buy_sell = [go.Scatter(x=df["time"], y=df["price"], name="Price")]

        # Buy signal annotations
        df2 = df.loc[df["buy"] == 1, ["time", "price"]]
        annotations = make_annotations(
            x=df2["time"],
            y=df2["price"],
            xref="x",
            yref="y",
            text="B",
            yanchor="top",
            color="black",
        )

        # Sell signal annotations
        df3 = df.loc[df["sell"] == 1, ["time", "price"]]
        sell_annotations = make_annotations(
            x=df3["time"],
            y=df3["price"],
            xref="x",
            yref="y",
            text="S",
            yanchor="bottom",
            color="red",
        )

        # Add the "sell" annotations to the "buy" annotations
        annotations += sell_annotations

        # Title for the chart
        title = f"Buy/Sell Signals for {ticker}"

        # Get the buy/sell signals chart
        chart_buy_sell = get_chart(
            traces_buy_sell, title, annotations=annotations, date_test=date_test
        )

        # Our profit is the number of shares we bought today, times the change in price tomorrow
        df["profit"] = df["position"] * df["delta_tm"]
        df["profit_cumul_train"] = df.loc[df["time"] < date_test, "profit"].cumsum()
        df["profit_cumul_test"] = df.loc[df["time"] >= date_test, "profit"].cumsum()

        traces_profits = [
            go.Scatter(
                x=df["time"],
                y=df["profit_cumul_train"],
                name="Training-Period Profit on 100 Shares",
                line=dict(color="MediumSeaGreen"),
            ),
            go.Scatter(
                x=df["time"],
                y=df["profit_cumul_test"],
                name="Test-Period Profit on 100 Shares",
                line=dict(color="DodgerBlue"),
            ),
        ]

        # Title for the chart
        title = f"Cumulative Profit on 100 Shares of {ticker}"

        # Get the buy/sell signals chart
        chart_profits = get_chart(
            traces_profits, title, date_test=date_test, rangeslider=False
        )

        # Make a message to display below the second chart, showing the final value of the portfolio
        ending_value_train = df.loc[df["time"] < date_test, "profit_cumul_train"].iloc[
            -1
        ]
        ending_value_test = df.loc[df["time"] >= date_test, "profit_cumul_test"].iloc[
            -1
        ]
        text_style_train = {"color": "black" if ending_value_train > 0 else "red"}
        text_style_test = {"color": "black" if ending_value_test > 0 else "red"}
        profits_chart_msg = html.Div(
            [
                html.P(
                    [
                        "Training Period Ending Value: ",
                        html.Span(f"{ending_value_train:,.0f}", style=text_style_train),
                    ]
                ),
                html.P(
                    [
                        "Testing Period Ending Value: ",
                        html.Span(f"{ending_value_test:,.0f}", style=text_style_test),
                    ]
                ),
            ],
            style={"text-align": "center"},
        )

        # How long did this callback take?
        seconds_elapsed = round(time.time() - time_start, 1)

        # Message to display below the "Train Model" button
        ml_model_label = ml_models_map.get(ml_model, "").lower()
        msg = f"{ticker} {ml_model_label} model trained in {seconds_elapsed} seconds!"
        current_app.logger.info(msg)

        return msg, chart_buy_sell, chart_profits, profits_chart_msg
