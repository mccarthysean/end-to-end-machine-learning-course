# /app/dashapp/utils.py

import datetime
from io import StringIO

import dash_core_components as dcc
import pandas as pd
import plotly.graph_objs as go
import yfinance as yf
from flask import current_app

from app.database import get_conn, run_sql_query

ml_models_map = dict(
    ols="Logistic Regression",
    ridge="Ridge Classifier",
    knn="K-Nearest Neighbors",
    dtab="AdaBoost Decision Tree",
    rf="Random Forest",
    sv="Support Vector Machine",
    mlp="Neural Network",
)

ml_features_map = dict(
    rt="Lagged Returns (6 days)",
    macd_binary="Moving Avg. Convergence Divergence (MACD)",
    macd_change_dod="MACD Change Day-Over-Day",
    RSI="Relative Strength Index (RSI)",
    CCI="Commodity Channel Index (CCI)",
    EMV="Ease of Movement (EMV)",
    FI="Force Index (FI)",
)


def get_stock_industries():
    """Get a list of different industries for which we have stock prices"""

    sql = """
        --Get the labels and underlying values for the dropdown menu "children"
        SELECT 
            distinct 
            industry as label,
            industry as value
        FROM public.stock_tickers;
    """

    rows, _ = run_sql_query(sql)

    if len(rows) == 0:
        first_value = None
    else:
        first_value = rows[0]["value"]

    return rows, first_value


def get_stock_tickers(industry):
    """Get a list of stocks based on the industry chosen"""

    sql = f"""
        --Get the labels and underlying values for the dropdown menu "children"
        SELECT 
            distinct 
            case when name is null then ticker else name end as label, 
            upper(ticker) as value
        FROM public.stock_tickers
        WHERE industry = '{industry}';
    """

    rows, _ = run_sql_query(sql)

    if len(rows) == 0:
        first_value = None
    else:
        first_value = rows[0]["value"]

    return rows, first_value


def get_stock_price_data_from_db(ticker):
    """
    Download the stock price data from the TimescaleDB database
    and return it in a Pandas DataFrame.
    """
    sql = f"""
        select
            time,
            ticker,
            round(close_adj::numeric, 2) as price,
            round(high::numeric, 2) as high,
            round(low::numeric, 2) as low,
            volume
        from public.stock_prices
        where ticker = '{ticker}'
        order by
            ticker,
            time;
    """

    rows, columns = run_sql_query(sql)

    # Return a DataFrame with the results
    return pd.DataFrame(rows, columns=columns)


def get_time_series_chart(ticker):
    """Get the normal time series chart of the stock price history"""

    df = get_stock_price_data_from_db(ticker)
    x = df["time"]
    y = df["price"]
    title = f"Historical Prices for {ticker}"

    traces = [go.Scatter(x=x, y=y, name="Price")]

    return get_chart(traces, title)


def insert_tickers(ticker, name=None, industry=None):
    """Insert the tickers into the "stock_tickers" table,
    and update them if they already exist"""

    sql = f"""
        insert into public.stock_tickers (ticker, name, industry)
        values ('{ticker}', '{name}', '{industry}')
        on conflict (ticker)
        do update
            set name = '{name}', industry = '{industry}'
    """

    conn = get_conn()
    with conn.cursor() as cursor:
        cursor.execute(sql)
        conn.commit()


def upload_to_aws_efficiently(df, ticker, table_name="public.stock_prices"):
    """
    Upload the stock price data to AWS as quickly and efficiently as possible
    by truncating (i.e. removing) the existing data and copying all-new data
    """

    conn = get_conn()
    with conn.cursor() as cursor:
        # Remove the existing data for that ticker
        cursor.execute(f"delete from {table_name} where ticker = '{ticker}'")
        conn.commit()

        # Now insert the brand-new data
        # Initialize a string buffer
        sio = StringIO()
        # Write the Pandas DataFrame as a CSV file to the buffer
        sio.write(df.to_csv(index=None, header=None))
        # Be sure to reset the position to the start of the stream
        sio.seek(0)
        cursor.copy_from(
            file=sio, table=table_name, sep=",", null="", size=8192, columns=df.columns
        )
        conn.commit()

    current_app.logger.info("DataFrame uploaded to TimescaleDB")


def download_prices(ticker, name=None, industry=None, period="10y", interval="1d"):
    """Download stock prices to a Pandas DataFrame, insert """

    stock = yf.Ticker(ticker)
    info = stock.info

    name = info.get("shortName", None)
    industry = info.get("sector", None)
    if industry is None:
        industry = info.get("category", None)

    # Update the tickers in the "stock_tickers" table
    insert_tickers(ticker, name=name, industry=industry)

    df = yf.download(tickers=ticker, period=period, interval=interval, progress=False)

    df = df.reset_index()  # remove the index
    df["ticker"] = ticker  # add a column for the ticker

    # Rename columns to match our database table
    df = df.rename(
        columns={
            "Date": "time",
            "Datetime": "time",
            "Open": "open",
            "High": "high",
            "Low": "low",
            "Close": "close",
            "Adj Close": "close_adj",
            "Volume": "volume",
        }
    )

    upload_to_aws_efficiently(df, ticker=ticker, table_name="public.stock_prices")

    return industry


def get_train_test_chart_annotations(date_test):
    """
    Draw a vertical line to visually separate the
    training period data from the testing period data.

    Also add some text annotations
    """

    # Convert date string to datetime object
    date_time = datetime.datetime.strptime(date_test, "%Y-%m-%d")

    # Add a vertical line to visually separate the
    # training period data from the testing period data
    shapes = [
        dict(
            type="line",
            xref="x",  # relative to x-axis values
            x0=date_time,
            x1=date_time,
            yref="paper",  # relative to pixels on chart
            y0=0,
            y1=1,
        )
    ]

    # Add two text annotations
    annotations = []
    # "Train" annotation
    annotations.append(
        dict(
            xref="x",  # relative to x-axis values
            x=date_time,
            yref="paper",  # relative to pixels on chart
            y=0.95,
            text="Train  ",
            showarrow=False,
            # xshift=-20,
            xanchor="right",
        )
    )
    # "Test" annotation
    annotations.append(
        dict(
            xref="x",  # relative to x-axis values
            x=date_time,
            yref="paper",  # relative to pixels on chart
            y=0.95,
            text="  Test",
            showarrow=False,
            # xshift=20,
            xanchor="left",
        )
    )

    return shapes, annotations


def get_chart(traces, title, annotations=None, date_test=None, rangeslider=True):
    """Get a Dash "Graph" object"""

    if annotations is None:
        annotations = []

    shapes = None
    if date_test is not None:
        shapes, more_annotations = get_train_test_chart_annotations(date_test)
        annotations += more_annotations

    figure = go.Figure(
        data=traces,
        layout=go.Layout(
            title=title,
            plot_bgcolor="white",
            annotations=annotations,
            shapes=shapes,
            legend=dict(
                # Looks much better horizontal than vertical
                orientation="h",
            ),
            xaxis=dict(
                autorange=True,
                # Time-filtering buttons above chart
                rangeselector=dict(
                    buttons=list(
                        [
                            dict(count=7, label="7d", step="day", stepmode="backward"),
                            dict(
                                count=14,
                                label="14d",
                                step="day",
                                stepmode="backward",
                            ),
                            dict(
                                count=1,
                                label="1m",
                                step="month",
                                stepmode="backward",
                            ),
                            dict(
                                count=3,
                                label="3m",
                                step="month",
                                stepmode="backward",
                            ),
                            dict(
                                count=1,
                                label="1y",
                                step="year",
                                stepmode="backward",
                            ),
                            dict(step="all"),
                        ]
                    )
                ),
                type="date",
                # Alternative time filter slider
                rangeslider=dict(visible=rangeslider),
            ),
        ),
    )

    return dcc.Graph(
        # Disable the ModeBar with the Plotly logo and other buttons
        config=dict(displayModeBar=False),
        figure=figure,
    )


def make_annotations(x, y, xref, yref, text, yanchor, color):
    """Make a list of annotation dictionaries,
    for annotating the charts with buy/sell indicators"""

    return [
        dict(
            x=x,
            y=y,
            xref=xref,
            yref=yref,
            text=text,
            yanchor=yanchor,
            font=dict(color=color),
        )
        for x, y in zip(x, y)
    ]
