# /app/dashapp/ml.py
import time

import numpy as np
import pandas as pd
from flask import current_app
from sklearn.ensemble import AdaBoostClassifier, RandomForestClassifier
from sklearn.linear_model import LogisticRegression, RidgeClassifier
from sklearn.model_selection import GridSearchCV, TimeSeriesSplit
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC


def feature_engineering(df):
    """Prepare the DataFrame for machine learning"""

    # Convert the numeric columns to numeric 'float64'
    df["price"] = pd.to_numeric(df["price"])
    df["high"] = pd.to_numeric(df["high"])
    df["low"] = pd.to_numeric(df["low"])
    df["volume"] = pd.to_numeric(df["volume"])

    # Feature Engineering
    # Create a new column with the price from the previous time period,
    # and back-fill the value so we don't have any null values
    df["price_prev"] = df["price"].shift(1).bfill()
    # Calculate the day-over-day price change
    df["delta"] = df["price"] - df["price_prev"]
    # Get tomorrow's price delta, which is how we'll calculate our profits
    df["delta_tm"] = df["delta"].shift(-1)
    # Calculate the percentage change in price, or the return
    df["rt"] = df["delta"] / df["price_prev"]
    # Get the lagged returns for more previous days, since there's autocorrelation
    df["rt1"] = df["rt"].shift(1)
    df["rt2"] = df["rt"].shift(2)
    df["rt3"] = df["rt"].shift(3)
    df["rt4"] = df["rt"].shift(4)
    df["rt5"] = df["rt"].shift(5)
    # Get the next day's return, which is ultimately what we're trying to forecast/predict
    df["rt_tm"] = df["rt"].shift(-1)
    # Make a simpler up/down indicator, for whether the return is positive or negative
    df["up_down_tm"] = np.where(df["rt_tm"] > 0, 1, 0)
    # Make an up-down indicator for the returns over the next week
    df["price_1wk"] = df["price"].shift(-7)
    df["up_down_1wk"] = np.where(df["price_1wk"] > df["price"], 1, 0)

    # Calculate the moving average convergence-divergence (MACD) technical indicator
    macd_12 = df["price"].ewm(span=12, adjust=False).mean()
    macd_26 = df["price"].ewm(span=26, adjust=False).mean()
    df["macd"] = macd_12 - macd_26
    df["macd_signal"] = df["macd"].ewm(span=9, adjust=False).mean()
    df["macd_delta"] = df["macd"] - df["macd_signal"]
    df["macd_change_dod"] = df["macd_delta"] - df["macd_delta"].shift(1)
    df["macd_binary"] = np.where(df["macd"] > df["macd_signal"], 1, 0)

    # Calculate the 14-day relative strength index (RSI) technical indicator
    # Window length for RSI moving average
    window_length = 14
    # Calculate the days where the price went up, otherwise zero
    up = pd.Series(np.where(df["delta"] > 0, df["delta"], 0))
    # Calculate the days where the price went down, otherwise zero
    down = pd.Series(np.where(df["delta"] < 0, df["delta"], 0))
    roll_up = up.rolling(window=window_length, min_periods=1).mean()
    roll_down = down.abs().rolling(window=window_length, min_periods=1).mean()
    df["RSI"] = 100.0 - (100.0 / (1.0 + (roll_up / roll_down)))

    # Calculate the 3-day commodity channel index (CCI) technical indicator
    df["CCI"] = (df["price"] - df["price"].rolling(3, min_periods=1).mean()) / (
        0.015 * df["price"].rolling(3, min_periods=1).std()
    )

    # Calculate the ease of movement (EVM) volume-based technical oscillator
    distance_moved = (df["high"] + df["low"]) / 2 - (
        df["high"].shift(1).bfill() + df["low"].shift(1).bfill()
    ) / 2
    box_ratio = (df["volume"] / 100_000_000) / (df["high"] - df["low"])
    df["EMV"] = distance_moved / box_ratio

    # Calculate the "force index" technical oscillator,
    # a measure of the "forcefullness" of the price change
    df["FI"] = df["delta"] * df["volume"]

    # Replace any infinite values with nulls (np.nan), and then drop all null/NA values
    current_app.logger.info(f"df.shape before removing bad values: {df.shape}")
    df = df.replace([np.inf, -np.inf], np.nan)
    df = df.dropna()
    current_app.logger.info(f"df.shape after removing bad values: {df.shape}")

    return df


def grid_search_cross_validate(
    X,
    y,
    name,
    estimator,
    param_grid,
    scoring="accuracy",
    cv=4,
    return_train_score=True,
    hyper_tune=True,
):
    """Perform grid-search hyper-parameter tuning and
    train/test cross-validation to prevent overfitting"""

    time_start = time.time()
    df = pd.DataFrame()
    if hyper_tune:
        estimator = GridSearchCV(
            estimator=estimator,
            param_grid=param_grid,
            scoring=scoring,
            cv=cv,
            return_train_score=return_train_score,
        )
        estimator.fit(X, y)
        df = pd.DataFrame(estimator.cv_results_)
        df["estimator"] = name
    else:
        # gs = GridSearchCV(estimator=estimator, param_grid=param_grid,
        #     scoring=scoring, cv=cv, return_train_score=return_train_score)
        estimator.fit(X, y)

    seconds_elapsed = time.time() - time_start
    current_app.logger.info(f"{name} took {round(seconds_elapsed)} seconds")

    return estimator, df


def train_models(X, y, n_splits, features, ml_model, hyper_tune):
    """Run all models (this will take a little while!)"""

    # Scale the data between 0 and 1
    # mms = MinMaxScaler(feature_range=(0,1))
    ss = StandardScaler()

    # Time series split for cross-validation
    tscv = TimeSeriesSplit(n_splits=n_splits)

    # Classification machine learning models to test
    ml_pipe_ols = Pipeline([("scale", ss), ("ols", LogisticRegression())])
    ml_pipe_ridge = Pipeline([("scale", ss), ("ridge", RidgeClassifier())])
    ml_pipe_dtab = Pipeline([("scale", ss), ("dtab", AdaBoostClassifier())])
    ml_pipe_rf = Pipeline([("scale", ss), ("rf", RandomForestClassifier())])
    ml_pipe_sv = Pipeline([("scale", ss), ("sv", SVC())])
    ml_pipe_knn = Pipeline([("scale", ss), ("knn", KNeighborsClassifier())])
    ml_pipe_mlp = Pipeline([("scale", ss), ("mlp", MLPClassifier())])

    # Classification hyper-parameter tuning grids
    param_grid_ols = [{"ols__fit_intercept": [True, False]}]
    param_grid_ridge = [
        {"ridge__alpha": [0, 0.001, 0.1, 1.0, 5, 10, 50, 100, 1000, 10000, 100000]}
    ]
    param_grid_dtab = [
        {
            "dtab__n_estimators": [50, 100],
            "dtab__learning_rate": [0.75, 1.0, 1.5],
        }
    ]
    param_grid_rf = [
        {
            "rf__n_estimators": [100, 200],
            "rf__max_features": ["auto", "sqrt", "log2"],
            "rf__max_depth": [2, 4, 8],
        }
    ]
    param_grid_mlp = [
        {
            "mlp__activation": ["relu", "tanh"],
            "mlp__solver": ["adam", "sgd"],
            "mlp__alpha": [0.1, 1, 10],
        }
    ]
    param_grid_sv = [
        {
            "sv__kernel": ["rbf", "linear"],
            "sv__C": [0.01, 0.1, 1, 10],
            "sv__gamma": [0.01, 0.1, 1],
        }
    ]
    param_grid_knn = [
        {
            "knn__n_neighbors": [8, 12, 16],
            "knn__weights": ["uniform", "distance"],
            "knn__p": [1, 2],
            "knn__n_jobs": [1, -1],
        }
    ]

    # Train the machine learning model
    estimator, df_grid_search_results = None, None
    if ml_model == "ols":
        estimator, df_grid_search_results = grid_search_cross_validate(
            X, y, "ols", ml_pipe_ols, param_grid_ols, cv=tscv, hyper_tune=hyper_tune
        )
    elif ml_model == "ridge":
        estimator, df_grid_search_results = grid_search_cross_validate(
            X,
            y,
            "ridge",
            ml_pipe_ridge,
            param_grid_ridge,
            cv=tscv,
            hyper_tune=hyper_tune,
        )
    elif ml_model == "knn":
        estimator, df_grid_search_results = grid_search_cross_validate(
            X, y, "knn", ml_pipe_knn, param_grid_knn, cv=tscv, hyper_tune=hyper_tune
        )
    elif ml_model == "dtab":
        estimator, df_grid_search_results = grid_search_cross_validate(
            X, y, "dtab", ml_pipe_dtab, param_grid_dtab, cv=tscv, hyper_tune=hyper_tune
        )
    elif ml_model == "rf":
        estimator, df_grid_search_results = grid_search_cross_validate(
            X, y, "rf", ml_pipe_rf, param_grid_rf, cv=tscv, hyper_tune=hyper_tune
        )
    elif ml_model == "sv":
        estimator, df_grid_search_results = grid_search_cross_validate(
            X, y, "sv", ml_pipe_sv, param_grid_sv, cv=tscv, hyper_tune=hyper_tune
        )
    elif ml_model == "mlp":
        estimator, df_grid_search_results = grid_search_cross_validate(
            X, y, "mlp", ml_pipe_mlp, param_grid_mlp, cv=tscv, hyper_tune=hyper_tune
        )

    return estimator, df_grid_search_results
