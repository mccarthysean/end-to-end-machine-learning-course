# /app/dashapp/layout.py

import datetime

import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html

from app.dashapp.utils import (
    get_stock_industries,
    get_stock_tickers,
    get_time_series_chart,
    ml_features_map,
    ml_models_map,
)


def get_navbar():
    """Get a Bootstrap 4 navigation bar for our single-page application's HTML layout"""

    return dbc.NavbarSimple(
        children=[
            dbc.NavItem(dbc.NavLink("Blog", href="https://mccarthysean.dev")),
            dbc.NavItem(dbc.NavLink("IJACK", href="https://myijack.com")),
            dbc.DropdownMenu(
                children=[
                    dbc.DropdownMenuItem("References", header=True),
                    dbc.DropdownMenuItem("Dash", href="https://dash.plotly.com/"),
                    dbc.DropdownMenuItem(
                        "Dash Bootstrap Components",
                        href="https://dash-bootstrap-components.opensource.faculty.ai/",
                    ),
                    dbc.DropdownMenuItem("Testdriven", href="https://testdriven.io/"),
                ],
                nav=True,
                in_navbar=True,
                label="Links",
            ),
            dbc.NavItem(
                dbc.Button(
                    "Logout",
                    href="/logout/",
                    external_link=True,
                    color="primary",
                    className="ml-2",
                )
            ),
        ],
        brand="Home",
        brand_href="/",
        color="dark",
        dark=True,
    )


def get_top_stock_selection_row(industries, industry, stocks_options, stock):
    """Get the first row of the layout"""

    # The layout starts with a Bootstrap row, containing a Bootstrap column
    return dbc.Row(
        [
            dbc.Col(
                [
                    html.H4("Pick an Industry", style={"margin-top": "1rem"}),
                    dcc.Dropdown(
                        options=industries,
                        value=industry,
                        id="industries_dropdown",
                    ),
                ],
                xs=12,
                sm=6,
                md=4,
            ),
            dbc.Col(
                [
                    html.H4("Pick a Stock", style={"margin-top": "1rem"}),
                    dcc.Dropdown(
                        options=stocks_options, value=stock, id="tickers_dropdown"
                    ),
                ],
                xs=12,
                sm=6,
                md=4,
            ),
            dbc.Col(
                [
                    html.H4("Download Fresh Stock Data", style={"margin-top": "1rem"}),
                    dbc.InputGroup(
                        [
                            dbc.Input(id="add_stock_input", placeholder="ticker"),
                            dbc.InputGroupAddon(
                                dbc.Button("Download", id="add_stock_input_button"),
                                addon_type="append",
                            ),
                        ]
                    ),
                    dbc.Spinner(
                        html.P(
                            html.Span(
                                id="stock_uploaded_msg", className="align-middle"
                            ),
                            style={"line-height": "2"},
                        )
                    ),
                ],
                xs=12,
                sm=6,
                md=4,
            ),
        ],
        style={"margin-top": "1em"},
    )


def get_regular_chart_row(stock_ticker):
    """Create a row and column for our Plotly/Dash time series chart"""

    return dbc.Row(
        dbc.Col(
            dbc.Spinner(
                [
                    html.Div(
                        get_time_series_chart(stock_ticker), id="time_series_chart_div"
                    )
                ]
            )
        )
    )


def get_ml_inputs_row():
    """Get the "machine learning inputs" row of the layout"""

    return dbc.Row(
        [
            dbc.Col(
                [
                    html.H4("Machine Learning Model", style={"margin-top": "1rem"}),
                    dbc.RadioItems(
                        id="ml_models_radio",
                        options=[
                            dict(label=value, value=key)
                            for key, value in ml_models_map.items()
                        ],
                        value="rf",
                    ),
                ],
                xs=12,
                sm=6,
                md=4,
            ),
            dbc.Col(
                [
                    html.H4(
                        "Explanatory Features to Use", style={"margin-top": "1rem"}
                    ),
                    dbc.Checklist(
                        id="ml_features_to_use",
                        options=[
                            dict(label=value, value=key)
                            for key, value in ml_features_map.items()
                        ],
                        # Just use all the available keys in the dictionary
                        value=list(ml_features_map.keys()),
                    ),
                ],
                xs=12,
                sm=6,
                md=4,
            ),
            dbc.Col(
                [
                    dbc.Row(
                        dbc.Col(
                            [
                                html.H4(
                                    "Do Hyper-Parameter Tuning",
                                    style={"margin-top": "1rem"},
                                ),
                                dbc.RadioItems(
                                    id="ml_do_hyper_param_tuning",
                                    options=[
                                        dict(
                                            label="Yes (takes longer, but better)",
                                            value=True,
                                        ),
                                        dict(label="No", value=False),
                                    ],
                                    value=False,
                                ),
                            ]
                        )
                    ),
                    dbc.Row(
                        dbc.Col(
                            [
                                html.H5(
                                    "Time Series Cross-Validation Splits",
                                    style={"margin-top": "2rem"},
                                ),
                                html.P("(more splits takes longer to train)"),
                                dcc.Dropdown(
                                    id="ml_cross_validation_splits",
                                    options=[
                                        dict(label="2", value=2),
                                        dict(label="3", value=3),
                                        dict(label="4", value=4),
                                        dict(label="5", value=5),
                                        dict(label="6", value=6),
                                        dict(label="7", value=7),
                                        dict(label="8", value=8),
                                    ],
                                    value=2,
                                ),
                            ]
                        )
                    ),
                ],
                xs=12,
                sm=6,
                md=4,
            ),
        ],
        style={"margin-top": "1em"},
    )


def get_train_model_button_row(stock_ticker):
    """Get the row of the layout that contains the
    test start date picker and the "Train Model" button"""

    # The max_test_start_date is the most recent date,
    # after which the test period should start
    max_test_start_date = datetime.date.today() - datetime.timedelta(days=14)

    # Defaults to 365 days ago. In other words, the model will train on data
    # before this date, and test on data on or after this date
    default_test_start_date = datetime.date.today() - datetime.timedelta(days=365)

    return dbc.Row(
        [
            dbc.Col(
                [
                    html.H4(
                        "Strategy",
                        style={"margin-top": "1rem"},
                    ),
                    dbc.RadioItems(
                        id="ml_strategy",
                        options=[
                            dict(
                                label="Long-only (no short-selling)",
                                value="lo",
                            ),
                            dict(label="Long-short (long or short)", value="ls"),
                        ],
                        # Default is long-only
                        value="lo",
                    ),
                ],
                xs=12,
                sm=6,
                md=4,
            ),
            dbc.Col(
                [
                    html.H4(
                        "Test Period Start Date",
                        style={"margin-top": "1rem"},
                    ),
                    html.P("(train on data before this date)"),
                    dcc.DatePickerSingle(
                        id="train_test_date_picker",
                        date=default_test_start_date,
                        max_date_allowed=max_test_start_date,
                        initial_visible_month=default_test_start_date,
                    ),
                    # Button to start the machine learning training process
                    dbc.Button(
                        "Train Model", id="train_ml_btn", color="dark", className="ml-2"
                    ),
                    dbc.Spinner(
                        html.P(
                            id="model_trained_msg",
                            style={"line-height": "2"},
                        ),
                    ),
                ],
                xs=12,
                sm=6,
                md=4,
            ),
        ],
        style={"margin-top": "1rem"},
    )


def get_ml_chart_row():
    """Create a row and column for our Plotly/Dash time series chart"""

    return dbc.Row(
        dbc.Col(
            dbc.Spinner(
                [
                    html.Div(
                        id="ml_chart_div",
                    ),
                ]
            )
        )
    )


def get_profits_chart_row():
    """Create a row and column for our cumulative profits time series chart"""

    return dbc.Row(
        dbc.Col(
            dbc.Spinner(
                [
                    html.Div(id="profits_chart_div"),
                    html.Div(id="profits_chart_msg_div"),
                ]
            )
        )
    )


def get_layout():
    """Function to get Dash's "HTML" layout"""

    industries, industry = get_stock_industries()
    stocks_options, stock = get_stock_tickers(industry)

    # A Bootstrap 4 container holds the rest of the layout
    return dbc.Container(
        [
            get_navbar(),
            get_top_stock_selection_row(industries, industry, stocks_options, stock),
            get_regular_chart_row(stock),
            get_ml_inputs_row(),
            get_train_model_button_row(stock),
            get_ml_chart_row(),
            get_profits_chart_row(),
        ]
    )
