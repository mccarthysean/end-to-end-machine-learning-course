import os

import click
import psycopg2
from flask import current_app, g
from flask.cli import with_appcontext
from psycopg2.extras import RealDictCursor


def get_conn():
    """
    Connect to the application's configured database. The connection
    is unique for each request and will be reused if this is called
    again.
    """
    if "conn" not in g:
        g.conn = psycopg2.connect(
            host=os.getenv("POSTGRES_HOST"),
            port=os.getenv("POSTGRES_PORT"),
            dbname=os.getenv("POSTGRES_DATABASE"),
            user=os.getenv("POSTGRES_USER"),
            password=os.getenv("POSTGRES_PASSWORD"),
            connect_timeout=5,
        )

    return g.conn


def run_sql_query(sql, conn=None):
    """Run a generic query and return the rows and columns"""

    if conn is None:
        conn = get_conn()

    with conn.cursor(cursor_factory=RealDictCursor) as cursor:
        cursor.execute(sql)
        columns = [str.lower(x[0]) for x in cursor.description]
        rows = cursor.fetchall()

    return rows, columns


def close_db(e=None):
    """
    If this request connected to the database, close the
    connection.
    """
    conn = g.pop("conn", None)

    if conn is not None:
        conn.close()

    return None


def recreate_db(db) -> None:
    """
    Create the TimescaleDB tables the app needs,
    the hyper-table, and the index.
    """

    # Ensure the db models are loaded, for db.create_all()

    try:
        current_app.logger.warning("Dropping existing database tables...")
        db.drop_all()

        current_app.logger.warning("Creating database tables...")
        db.create_all()

        sql_create_hypertable = """
            SELECT create_hypertable('stock_prices', 'time', migrate_data => true);
        """
        db.session.execute(sql_create_hypertable)

        sql_create_hypertable_index = """
            create index on stock_prices (ticker, time desc);
        """
        db.session.execute(sql_create_hypertable_index)
    except Exception:
        current_app.logger.exception("ERROR recreating database tables!")
        db.session.rollback()
    else:
        db.session.commit()
        current_app.logger.info("Database tables created successfully!")

    return None


def check_db_tables(app, db):
    """
    On startup, quickly check the database tables.
    If the stock prices table don't exist, recreate them all.
    """
    app.logger.info("Checking whether database tables exist...")
    sql = """
        SELECT EXISTS (
            SELECT FROM information_schema.tables 
            WHERE  table_schema = 'public'
            AND    table_name   = 'stock_prices'
        );
    """
    with app.app_context():
        rows, _ = run_sql_query(sql)
        if rows[0]["exists"] is False:
            recreate_db(db)


@click.command("init-db")
@with_appcontext
def init_db():
    """
    Create the TimescaleDB tables the app needs,
    the hyper-table, and the index.

    This function can be run from the command line with "flask init-db"
    """
    from app import db

    recreate_db(db)


def init_app(app):
    """
    Register database functions with the Flask app. This is called by
    the application factory create_app().
    """
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db)
