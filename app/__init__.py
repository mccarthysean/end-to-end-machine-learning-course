# /app/__init__.py

import logging
import os

# Third-party imports
from flask import Flask
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

# Local imports
from app import database
from app.dash_setup import register_dashapp
from app.views import register_views

db = SQLAlchemy()
login_manager = LoginManager()
# logging.basicConfig(level=logging.DEBUG)


def create_app():
    """Factory function that creates the Flask app"""

    app = Flask(__name__)
    # Set a few configuration variables
    app.config["SECRET_KEY"] = os.getenv("SECRET_KEY")
    app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("SQLALCHEMY_DATABASE_URI")
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    # TimescaleDB needs the following SQLAlchemy options to be set,
    # for the connection pool, so it doesn't time out and cause a 500 server error
    app.config["SQLALCHEMY_ENGINE_OPTIONS"] = {
        # pre-ping to see if the connection is still available, or should be recycled
        "pool_pre_ping": True,
        "pool_size": 10,  # 10 connections in the pool
        "pool_timeout": 10,  # seconds
        "pool_recycle": 300,  # seconds
    }

    if not app.debug and app.env != "development":
        # Use the Gunicorn logger instead of setting up our own
        gunicorn_logger = logging.getLogger("gunicorn.error")
        app.logger.handlers = gunicorn_logger.handlers
        app.logger.setLevel(gunicorn_logger.level)
        # # In production mode, add log handler to sys.stderr.
        # app.logger.addHandler(logging.StreamHandler())
        # app.logger.setLevel(logging.INFO)
        app.logger.info("StocksML demo starting now...")

    # Initialize extensions
    db.init_app(app)  # SQLAlchemy for database management
    Bootstrap(app)  # Flask-Bootstrap for easy styling
    login_manager.init_app(app)  # Flask-Login

    # The name of the log-in view for Flask-Login
    login_manager.login_view = "login"

    # Register the Flask homepage, login, register, and logout views
    register_views(app)

    # Ensure the database tables exist. If not, create them
    database.init_app(app)
    database.check_db_tables(app, db)

    # Register the Dash app, after ensuring the database tables exist
    dashapp = register_dashapp(app)

    return app, dashapp
