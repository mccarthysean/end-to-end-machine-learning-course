CREATE TABLE IF NOT EXISTS stock_tickers (
  ticker TEXT PRIMARY KEY,
  name TEXT,
  industry TEXT
);

CREATE TABLE IF NOT EXISTS stock_prices (
  time TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  ticker TEXT NOT NULL,
  open NUMERIC,
  high NUMERIC,
  low NUMERIC,
  close NUMERIC,
  close_adj NUMERIC NOT NULL,
  volume NUMERIC NOT NULL,
  PRIMARY KEY (time, ticker),
  FOREIGN KEY (ticker) REFERENCES stock_tickers (ticker)
);

CREATE TABLE IF NOT EXISTS users (
    id SERIAL,
    email TEXT NOT NULL,
    first_name TEXT NOT NULL,
    last_name TEXT NOT NULL,
    password_hash TEXT NOT NULL,
    last_login_at TIMESTAMP WITHOUT TIME ZONE,
    login_count INTEGER,
    PRIMARY KEY (id)
);

-- Run the following manually, to create a TimescaleDB hyper-table
-- SELECT create_hypertable('stock_prices', 'time', migrate_data => true);
-- CREATE INDEX ON stock_prices (ticker, time desc);