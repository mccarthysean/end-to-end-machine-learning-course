# End to End Machine Learning for Stock Price Forecasting

This repo is for the "End to End Machine Learning" course being developed by Sean McCarthy.

### Outline

1. Get time series stock price data into TimescaleDB database, managed with PGAdmin, in Docker containers
2. Machine learning for stock price forecasting
   1. Data pre-processing, visualization, and scaling to prepare for machine learning
   2. Feature engineering to create potential explanatory variables for predicting stock prices
   3. Feature selection techniques to find the features that best predict future stock prices
   4. Hyper-parameter tuning to optimize several different machine learning models
   5. Time series cross-validation to train the machine learning models, and test on separate datasets to prevent overfitting
   6. Scoring and selection of best machine learning model
3. Integrate Dash and Flask to use React single page application technology with only Python
4. Publish machine learning model to Dash app, and learn about Dash layouts and callbacks for interactivity
5. Deploy application to cloud server with Traefik web server and Docker Swarm