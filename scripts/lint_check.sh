#!/bin/bash

# Enable exit on non 0
set -e
set -x

# Set the current working directory to the directory in which the script is located, for CI/CD
cd "$(dirname "$0")"
# cd ..
echo "Current working directory: $(pwd)"

# Nice sorting of imports
echo ""
echo "Running isort..."
isort --profile black ../app --check-only
isort --profile black ../tests --check-only

# Remove unused imports and unused variables
echo ""
echo "Running autoflake..."
autoflake --in-place --remove-unused-variables --remove-all-unused-imports --verbose --recursive ../app
autoflake --in-place --remove-unused-variables --remove-all-unused-imports --verbose --recursive ../tests

# Opinionated but lovely auto-formatting
echo ""
echo "Running black..."
black ../app --check
black ../tests --check

echo ""
echo "Running flake8..."
# flake8 ../app
# flake8 ../tests
flake8 ../app --ignore 'E402,E501,W503,E203,E741,C901'
flake8 ../tests --ignore 'E402,E501,W503,E203,E741,C901'

# echo ""
# echo "Running mypy..."
# mypy --config-file ../mypy.ini ../app --disallow-untyped-defs

# Security checks with Bandit and Safety
echo ""
echo "Running bandit..."
bandit -r "../app"
bandit -r "../tests" --configfile "../.bandit_4_tests.yml"

echo ""
echo "Running safety..."
safety check

# For Jinja2 template blocks
echo ""
echo "Running curlylint..."
curlylint ../app/templates --parse-only

echo ""
echo "Lint check complete!"

exit 0